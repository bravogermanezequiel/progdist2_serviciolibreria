FROM python:3.9
WORKDIR /libreria
COPY . /libreria/
RUN pip install -r requirements.txt