from flask import jsonify
from entidades import *
from app import db
from excepciones import *

class RootRepositorio:
    def get(self):
        return {
            "Servidor": "TP Final Programacion Distribuida I - German Ezequiel Bravo"
        }
        
class AutorRepositorio:
    def list(self):
        autores = Autor.query.order_by(Autor.id).all()
    
        return {
            "autores": [
                {
                    "id": x.id, 
                    "nombre": x.nombre, 
                    "apellido": x.apellido, 
                    "edad": x.edad
                } for x in autores
            ]
        }
    
    def get(self, id):
        autor = Autor.query.get(id)
    
        if autor is None:
            raise NotFoundException
            
        return {
            "id": autor.id,
            "nombre": autor.nombre,
            "apellido": autor.apellido,
            "edad": autor.edad
        }

class GeneroRepositorio:
    def list(self):
        generos = Genero.query.order_by(Genero.id).all()

        return {
            "generos": [
                {
                    "id": x.id, 
                    "nombre": x.nombre
                } for x in generos
            ]
        }
    
    def get(self, id):
        genero = Genero.query.get(id)
    
        if genero is None:
            raise NotFoundException
            
        return {
            "id": genero.id,
            "nombre": genero.nombre
        }
    
class EditorialRepositorio:
    def list(self):
        editoriales = Editorial.query.order_by(Editorial.id).all()

        return {
            "editoriales": [
                {
                    "id": x.id, 
                    "nombre": x.nombre
                } for x in editoriales
            ]
        }
    
    def get(self, id):
        editorial = Editorial.query.get(id)
    
        if editorial is None:
            raise NotFoundException
        
        return {
            "id": editorial.id,
            "nombre": editorial.nombre
        }

class LibroRepositorio:
    def list(self):
        libros = Libro.query.order_by(Libro.id).all()
    
        return {
            "libros": [
                {
                    "id": x.id,
                    "titulo": x.titulo,
                    "autorId": x.autorId,
                    "generoId": x.generoId,
                    "editorialId": x.editorialId,
                    "volumen": x.volumen
                } for x in libros
            ]
        }
        
    def get(self, id):
        libro = Libro.query.get(id)
    
        if libro is None:
            raise NotFoundException
            
        return {
            "id": libro.id, 
            "titulo": libro.titulo,
            "autorId": libro.autorId,
            "generoId": libro.generoId,
            "editorialId": libro.editorialId,
            "volumen": libro.volumen
        }
    
    def create(self):
        from flask import request
        json = request.get_json()
        titulo = json.get("titulo")
        autorId = json.get("autorId")
        generoId = json.get("generoId")
        editorialId = json.get("editorialId")
        volumen = json.get("volumen")
        
        if not volumen in range(1,5): #1..4
            raise BadRequestException("Volumenes permitidos 1, 2, 3, 4")

        newLibro = Libro(titulo, autorId, generoId, editorialId, volumen)
        db.session.add(newLibro)
        db.session.commit()
            
        return {
            "id": newLibro.id
        }
    
    def update(self, id):
        libro = Libro.query.get(id)
    
        if libro is None:
            raise NotFoundException
        
        columnas = libro.__table__.columns.keys()[1:] # Se seleccionan las columnas que nos interesan, es decir todas menos "id" (0) pues no se puede o debe actualizar ya que corresponde como identificador al recurso
        
        from flask import request
        json = request.get_json()
        
        for columna in columnas:
            setattr(libro, columna, json.get(columna))
                
        db.session.add(libro)
        db.session.commit()
            
        return {
            "id": libro.id
        }
    
    def delete(self, id):
        libro = Libro.query.get(id)
    
        if libro is None:
            raise NotFoundException
                
        db.session.delete(libro)
        db.session.commit()

        return {
            "id": libro.id
        }

class SocioRepositorio:
    def list(self):
        socios = Socio.query.order_by(Socio.id).all()
    
        return {
            "socios": [
                {
                    "id": x.id,
                    "nombre": x.nombre,
                    "apellido": x.apellido,
                    "edad": x.edad,
                } for x in socios
            ]
        }
    
    def get(self, id):
        socio = Socio.query.get(id)
    
        if socio is None:
            raise NotFoundException
            
        return {
            "id": socio.id,
            "nombre": socio.nombre,
            "apellido": socio.apellido,
            "edad": socio.edad
        }
    
    def create(self):
        from flask import request
        json = request.get_json()
        nombre = json.get("nombre")
        apellido = json.get("apellido")
        edad = json.get("edad")
        
        newSocio = Socio(nombre, apellido, edad)

        db.session.add(newSocio)
        db.session.commit()
            
        return {
            "id": newSocio.id
        }
    
    def update(self, id):
        socio = Socio.query.get(id)
    
        if socio is None:
            raise NotFoundException
        
        columnas = socio.__table__.columns.keys()[1:] # Se seleccionan las columnas que nos interesan, es decir todas menos "id" (0) pues no se puede o debe actualizar ya que corresponde como identificador al recurso
        
        from flask import request
        json = request.get_json()
        
        for columna in columnas:
            setattr(socio, columna, json.get(columna))
                
        db.session.add(socio)
        db.session.commit()

        return {
            "id": socio.id
        }
    
    def delete(self, id):
        socio = Socio.query.get(id)
    
        if socio is None:
            raise NotFoundException
        
        db.session.delete(socio)
        db.session.commit()
            
        return {
            "id": socio.id
        }