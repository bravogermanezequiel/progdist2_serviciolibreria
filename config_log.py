import sys
from os import environ

CONFIG_LOGGING = {
    'version': 1,
    'formatters': { 
        'standard': {
            'format': f'%(asctime)s service:libreria containerid:{environ.get("HOSTNAME")} level:%(levelname)s: %(message)s',
            'datefmt': '%Y-%m-%d - %H:%M:%S' },
    },
    'handlers': {
        'console':  {'class': 'logging.StreamHandler', 
                    'formatter': "standard", 
                    'level': 'DEBUG', 
                    'stream': sys.stdout},
        'file_receiverrequest':     {'class': 'logging.FileHandler', 
                    'formatter': "standard", 
                    'level': 'DEBUG', 
                    'filename': 'log/receiverequest.log','mode': 'a'}
    },
    'loggers': { 
        'RECEIVEREQUEST_LOGGING':   {'level': 'INFO', 
                    'handlers': ['console', 'file_receiverrequest'], 
                    'propagate': False }
    }
}