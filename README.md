# API Libreria

## Entidades

- Autor
    1. id
    2. nombre
    3. apellido
    4. edad
- Genero
    1. id
    2. nombre
- Editorial
    1. id
    2. nombre
- Libro
    1. id
    2. titulo
    3. autorId
    4. generoId
    5. editorialId
    6. volumen
- Socio
    1. id
    2. nombre
    3. apellido
    4. edad

## Rutas

- Autor
    1. Listas autores:  GET /autores/
    2. Obtener autor:   GET /autor/{int:id}
- Genero
    1. Listar generos:  GET /generos/
    2. Obtener genero:  GET /genero/{int:id}
- Editorial:
    1. Listar editoriales: GET /editoriales/
    2. Obtener editorial: GET /editorial/{int:id}
- Libro
    1. Listar libros:   GET /libros/
    2. Obtener libro:   GET /libro/{int:id}
    2. Crear libro:     POST /libro/
    4. Actualizar libro: PUT /libro/{int:id}
    5. Eliminar libro: DELETE /libro/{int:id}
- Socio
    1. Listar socios:   GET /socios/
    2. Obtener socio:   GET /socio/{int:id}
    3. Crear socio:     POST /socio/
    4. Actualizar socio: PUT /socio/{int:id}
    5. Eliminar socio: DELETE /socio/{int:id}
