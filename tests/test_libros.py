from . import BaseTestClass
from app import db
from entidades import Libro, Autor, Genero, Editorial
import json

class TestLibrosCase(BaseTestClass):
    
    def setUp(self):
        super().setUp()
        
        with self.app.app_context():
            autor1Data = {"nombre": "Juan", "apellido": "Perez", "edad": 50}
            autor1 = Autor(**autor1Data)
            db.session.add(autor1)
            
            autor2Data = {"nombre": "German", "apellido": "Bravo", "edad": 30}
            autor2 = Autor(**autor2Data)
            db.session.add(autor2)
            
            genero1Data = {"nombre": "Policial"}
            genero1 = Genero(**genero1Data)
            db.session.add(genero1)
            
            genero2Data = {"nombre": "Romantico"}
            genero2 = Genero(**genero2Data)
            db.session.add(genero2)
            
            editorial1Data = {"nombre": "Larousse"}
            editorial1 = Editorial(**editorial1Data)
            db.session.add(editorial1)
            
            editorial2Data = {"nombre": "Pixxel"}
            editorial2 = Editorial(**editorial2Data)
            db.session.add(editorial2)
            
            db.session.commit()
    
    def testListLibros(self):
        with self.app.app_context():
            libro1Data = {"titulo": "Kobb's army", "generoId": 1, "autorId": 1, "editorialId": 1, "volumen": 2}
            libro1 = Libro(**libro1Data)
            db.session.add(libro1)
            
            libro2Data = {"titulo": "Pepito la perinola", "generoId": 2, "autorId": 2, "editorialId": 2, "volumen": 1}
            libro2 = Libro(**libro2Data)
            db.session.add(libro2)
            
            db.session.commit()
            
            res = self.client.get('/libros/')
            
            jsonData = json.loads(res.data.decode())
            
            assert res.status_code == 200
            assert jsonData["libros"][0].get("titulo") == libro1Data.get("titulo")
            assert jsonData["libros"][0].get("generoId") == libro1Data.get("generoId")
            assert jsonData["libros"][0].get("autorId") == libro1Data.get("autorId")
            assert jsonData["libros"][0].get("editorialId") == libro1Data.get("editorialId")
            assert jsonData["libros"][0].get("volumen") == libro1Data.get("volumen")
            assert jsonData["libros"][1].get("titulo") == libro2Data.get("titulo")
            assert jsonData["libros"][1].get("generoId") == libro2Data.get("generoId")
            assert jsonData["libros"][1].get("autorId") == libro2Data.get("autorId")
            assert jsonData["libros"][1].get("editorialId") == libro2Data.get("editorialId")
            assert jsonData["libros"][1].get("volumen") == libro2Data.get("volumen")


    def testGetDefinedLibro(self):
        with self.app.app_context():
            libro1Data = {"titulo": "Kobb's army", "generoId": 1, "autorId": 1, "editorialId": 1, "volumen": 2}
            libro1 = Libro(**libro1Data)
            db.session.add(libro1)
            
            db.session.commit()
            
            res = self.client.get('/libro/1')
            
            jsonData = json.loads(res.data.decode())
            
            assert res.status_code == 200
            assert jsonData.get("id") == 1
            assert jsonData.get("titulo") == libro1Data.get("titulo")
            assert jsonData.get("generoId") == libro1Data.get("generoId")
            assert jsonData.get("autorId") == libro1Data.get("autorId")
            assert jsonData.get("editorialId") == libro1Data.get("editorialId")
            assert jsonData.get("volumen") == libro1Data.get("volumen")
    
    def testGetUndefinedLibro(self):
        with self.app.app_context():
            libro1Data = {"titulo": "Kobb's army", "generoId": 1, "autorId": 1, "editorialId": 1, "volumen": 2}
            libro1 = Libro(**libro1Data)
            db.session.add(libro1)
            
            db.session.commit()
            
            res = self.client.get('/libro/10')
            
            jsonData = json.loads(res.data.decode())
            
            assert res.status_code == 404
            assert jsonData.get("Resultado") == "No encontrado"
    
    def testCreateLibro(self):
        with self.app.app_context():
            libro1Data = {"titulo": "Kobb's army", "generoId": 1, "autorId": 1, "editorialId": 1, "volumen": 2}
            
            res = self.client.post('/libro/', data=json.dumps(libro1Data), content_type='application/json')
            
            assert res.status_code == 201
            
            jsonData = json.loads(res.data.decode())
            assert jsonData.get("id") == 1
            
            libro = Libro.query.get(1)
            assert libro.titulo == libro1Data.get("titulo")
            assert libro.generoId == libro1Data.get("generoId")
            assert libro.autorId == libro1Data.get("autorId")
            assert libro.editorialId == libro1Data.get("editorialId")
            assert libro.volumen == libro1Data.get("volumen")
    
    def testUpdateDefinedLibro(self):
        with self.app.app_context():
            libro1Data = {"titulo": "Kobb's army", "generoId": 1, "autorId": 1, "editorialId": 1, "volumen": 2}
            libro1 = Libro(**libro1Data)
            db.session.add(libro1)
            
            newLibro1Data = {"titulo": "Kukelo", "generoId": 2, "autorId": 2, "editorialId": 2, "volumen": 1}
            
            res = self.client.put('/libro/1', data=json.dumps(newLibro1Data), content_type='application/json')
            
            
            jsonData = json.loads(res.data.decode())
            assert res.status_code == 200
            assert jsonData.get("id") == 1
            
            libro = Libro.query.get(1)
            assert libro.titulo == newLibro1Data.get("titulo")
            assert libro.generoId == newLibro1Data.get("generoId")
            assert libro.autorId == newLibro1Data.get("autorId")
            assert libro.editorialId == newLibro1Data.get("editorialId")
            assert libro.volumen == newLibro1Data.get("volumen")

    def testUpdateUndefinedLibro(self):
        with self.app.app_context():
            libro1Data = {"titulo": "Kukelo", "generoId": 2, "autorId": 2, "editorialId": 2, "volumen": 1}
            
            res = self.client.put('/libro/1', data=json.dumps(libro1Data), content_type='application/json')
            
            
            jsonData = json.loads(res.data.decode())
            assert res.status_code == 404
            assert jsonData.get("Resultado") == "No encontrado"
    
    def testDeleteDefinedLibro(self):
        with self.app.app_context():
            libro1Data = {"titulo": "Kobb's army", "generoId": 1, "autorId": 1, "editorialId": 1, "volumen": 1}
            libro1 = Libro(**libro1Data)
            db.session.add(libro1)
            
            res = self.client.delete('/libro/1')
            
            jsonData = json.loads(res.data.decode())
            assert res.status_code == 200
            assert jsonData.get("id") == 1

    def testDeleteUndefinedLibro(self):
        with self.app.app_context():            
            res = self.client.delete('/libro/1')
            
            
            jsonData = json.loads(res.data.decode())
            assert res.status_code == 404
            assert jsonData.get("Resultado") == "No encontrado"