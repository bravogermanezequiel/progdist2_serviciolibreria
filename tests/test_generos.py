from . import BaseTestClass
from app import db
from entidades import Genero
import json

class TestGenerosCase(BaseTestClass):
    def testListGeneros(self):
        with self.app.app_context():
            genero1Data = {"nombre": "Policial"}
            genero1 = Genero(**genero1Data)
            db.session.add(genero1)
            
            genero2Data = {"nombre": "Romantico"}
            genero2 = Genero(**genero2Data)
            db.session.add(genero2)
            
            db.session.commit()
            
            res = self.client.get('/generos/')
            
            jsonData = json.loads(res.data.decode())
            
            assert res.status_code == 200
            assert jsonData["generos"][0].get("nombre") == genero1Data.get("nombre")
            assert jsonData["generos"][1].get("nombre") == genero2Data.get("nombre")
    
    def testGetDefinedGenero(self):
        with self.app.app_context():
            genero1Data = {"nombre": "Policial"}
            genero1 = Genero(**genero1Data)
            db.session.add(genero1)
            
            db.session.commit()
            
            res = self.client.get('/genero/1')
            
            jsonData = json.loads(res.data.decode())
            
            assert res.status_code == 200
            assert jsonData.get("id") == 1
            assert jsonData.get("nombre") == genero1Data.get("nombre")
    
    def testGetUndefinedGenero(self):
        with self.app.app_context():
            genero1Data = {"nombre": "Policial"}
            genero1 = Genero(**genero1Data)
            db.session.add(genero1)
            
            db.session.commit()
            
            res = self.client.get('/genero/10')
            
            jsonData = json.loads(res.data.decode())
            
            assert res.status_code == 404
            assert jsonData.get("Resultado") == "No encontrado"
    
            
        