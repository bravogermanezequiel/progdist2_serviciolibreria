from . import BaseTestClass

class TestRootCase(BaseTestClass):
    def testGetRoot(self):
        res = self.client.get('/')
        self.assertEqual(200, res.status_code)
        self.assertIn(b'TP Final Programacion Distribuida I - German Ezequiel Bravo', res.data)