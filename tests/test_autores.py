from . import BaseTestClass
from app import db
from entidades import Autor
import json

class TestAutoresCase(BaseTestClass):
    def testListAutores(self):
        with self.app.app_context():
            autor1Data = {"nombre": "Juan", "apellido": "Perez", "edad": 50}
            autor1 = Autor(**autor1Data)
            db.session.add(autor1)
            
            autor2Data = {"nombre": "German", "apellido": "Bravo", "edad": 30}
            autor2 = Autor(**autor2Data)
            db.session.add(autor2)
            
            db.session.commit()
            
            res = self.client.get('/autores/')
            
            jsonData = json.loads(res.data.decode())
            
            assert res.status_code == 200
            assert jsonData["autores"][0].get("nombre") == autor1Data.get("nombre")
            assert jsonData["autores"][0].get("apellido") == autor1Data.get("apellido")
            assert jsonData["autores"][0].get("edad") == autor1Data.get("edad")
            assert jsonData["autores"][1].get("nombre") == autor2Data.get("nombre")
            assert jsonData["autores"][1].get("apellido") == autor2Data.get("apellido")
            assert jsonData["autores"][1].get("edad") == autor2Data.get("edad")
    
    def testGetDefinedAutor(self):
        with self.app.app_context():
            autor1Data = {"nombre": "Juan", "apellido": "Perez", "edad": 50}
            autor1 = Autor(**autor1Data)
            db.session.add(autor1)
            
            db.session.commit()
            
            res = self.client.get('/autor/1')
            
            jsonData = json.loads(res.data.decode())
            
            assert res.status_code == 200
            assert jsonData.get("id") == 1
            assert jsonData.get("nombre") == autor1Data.get("nombre")
            assert jsonData.get("apellido") == autor1Data.get("apellido")
            assert jsonData.get("edad") == autor1Data.get("edad")
    
    def testGetUndefinedAutor(self):
        with self.app.app_context():
            autor1Data = {"nombre": "Juan", "apellido": "Perez", "edad": 50}
            autor1 = Autor(**autor1Data)
            db.session.add(autor1)
            
            db.session.commit()
            
            res = self.client.get('/autor/10')
            
            jsonData = json.loads(res.data.decode())
            
            assert res.status_code == 404
            assert jsonData.get("Resultado") == "No encontrado"
    
            
        