from . import BaseTestClass
from app import db
from entidades import Editorial
import json

class TestEditorialesCase(BaseTestClass):
    def testListEditoriales(self):
        with self.app.app_context():
            editorial1Data = {"nombre": "Larousse"}
            editorial1 = Editorial(**editorial1Data)
            db.session.add(editorial1)
            
            editorial2Data = {"nombre": "Pixxel"}
            editorial2 = Editorial(**editorial2Data)
            db.session.add(editorial2)
            
            db.session.commit()
            
            res = self.client.get('/editoriales/')
            
            jsonData = json.loads(res.data.decode())
            
            assert res.status_code == 200
            assert jsonData["editoriales"][0].get("nombre") == editorial1Data.get("nombre")
            assert jsonData["editoriales"][1].get("nombre") == editorial2Data.get("nombre")
    
    def testGetDefinedEditorial(self):
        with self.app.app_context():
            editorial1Data = {"nombre": "Larousse"}
            editorial1 = Editorial(**editorial1Data)
            db.session.add(editorial1)
            
            db.session.commit()
            
            res = self.client.get('/editorial/1')
            
            jsonData = json.loads(res.data.decode())
            
            assert res.status_code == 200
            assert jsonData.get("id") == 1
            assert jsonData.get("nombre") == editorial1Data.get("nombre")
    
    def testGetUndefinedEditorial(self):
        with self.app.app_context():
            editorial1Data = {"nombre": "Larousse"}
            editorial1 = Editorial(**editorial1Data)
            db.session.add(editorial1)
            
            db.session.commit()
            
            res = self.client.get('/editorial/10')
            
            jsonData = json.loads(res.data.decode())
            
            assert res.status_code == 404
            assert jsonData.get("Resultado") == "No encontrado"
    
            
        