## ¿Que tests se realizaron?

<p>Se llevaron a cabo tests sobre todas las entidades y operaciones referidas a verbos API de forma unitaria. Para todo se verifica la salida y codigo de respuesta esperados.</p>

<h3>Autor</h3>

- Listado de autores: se realizo prueba cargando 2 autores por medio del ORM, y luego obtenerlo por medio de <b>GET /autores</b>

- Obtencion de autor existente: se realizo prueba cargando 1 autor por medio del ORM, y luego obterlo por medio de <b>GET /autor/1</b>

- Obtencion de autor no existente: se realizo prueba obteniendo el autor con id 10 por medio de <b>GET /autor/10</b>

<h3>Genero</h3>

- Listado de generos: se realizo prueba cargando 2 generos por medio del ORM, y luego obtenerlo por medio de <b>GET /generos</b>

- Obtencion de genero existente: se realizo prueba cargando 1 genero por medio del ORM, y luego obterlo por medio de <b>GET /genero/1</b>

- Obtencion de genero no existente: se realizo prueba obteniendo el genero con id 10 por medio de <b>GET /genero/10</b>

<h3>Editorial</h3>

- Listado de editoriales: se realizo prueba cargando 2 editoriales por medio del ORM, y luego obtenerlo por medio de <b>GET /editoriales</b>

- Obtencion de editorial existente: se realizo prueba cargando 1 editorial por medio del ORM, y luego obterlo por medio de <b>GET /editorial/1</b>

- Obtencion de editorial no existente: se realizo prueba obteniendo el editorial con id 10 por medio de <b>GET /editorial/10</b>

<h3>Libro</h3>

- En principio se realizó pre carga de entidades de autor, genero, y editorial, necesarios para luego poder crear las entidades libro.

- Listado de libros: se realizo prueba cargando 2 libros por medio del ORM, y luego obtenerlo por medio de <b>GET /libros</b>

- Obtencion de libro existente: se realizo prueba cargando 1 libro por medio del ORM, y luego obterlo por medio de <b>GET /libro/1</b>

- Obtencion de libro no existente: se realizo prueba obteniendo el libro con id 10 por medio de <b>GET /libro/10</b>

- Creacion de libro: se procede a crear un libro por medio de <b>POST /libro</b> con datos en el cuerpo definidos y luego se verifica contra la entidad del ORM, obteniendo el primer libro.

- Actualización de libro existente: se procede a crear un libro por medio del ORM, y luego por medio de <b>PUT /libro/1</b> con datos en el cuerpo definidos, se intenta actualizar el libro. Para finalizar se verifica los datos que se enviaron al PUT con aquellos obtenidos por medio del ORM nuevamente.

- Actualización de libro no existente: se procede a actualizar un libro por medio de <b>PUT /libro/1</b> con datos en el cuerpo definidos, se intenta actualizar el libro. Para finalizar se verifica que la respuesta sea la tipica de no encontrado.

- Eliminacion de libro existente: por medio del ORM se crea un libro y luego a través del endpoint <b>DELETE /libro/1</b>, se procede a eliminarlo, por ultimo se verifica lo devuelto y si por medio del ORM ya no existen libros cargados.

- Eliminacion de libro no existente: a través del endpoint <b>DELETE /libro/1</b> se procede a eliminar el libro con id 1 (inexistente), por lo que se verifica que la salida sea adecuada a un mensaje de no encontrado.

<h3>Socio</h3>

- Listado de socios: se realizo prueba cargando 2 socios por medio del ORM, y luego obtenerlo por medio de <b>GET /socios</b>

- Obtencion de socio existente: se realizo prueba cargando 1 socio por medio del ORM, y luego obterlo por medio de <b>GET /socio/1</b>

- Obtencion de socio no existente: se realizo prueba obteniendo el socio con id 10 por medio de <b>GET /socio/10</b>

- Creacion de socio: se procede a crear un socio por medio de <b>POST /socio</b> con datos en el cuerpo definidos y luego se verifica contra la entidad del ORM, obteniendo el primer socio.

- Actualización de socio existente: se procede a crear un socio por medio del ORM, y luego por medio de <b>PUT /socio/1</b> con datos en el cuerpo definidos, se intenta actualizar el socio. Para finalizar se verifica los datos que se enviaron al PUT con aquellos obtenidos por medio del ORM nuevamente.

- Actualización de socio no existente: se procede a actualizar un socio por medio de <b>PUT /socio/1</b> con datos en el cuerpo definidos, se intenta actualizar el socio. Para finalizar se verifica que la respuesta sea la tipica de no encontrado.

- Eliminacion de socio existente: por medio del ORM se crea un socio y luego a través del endpoint <b>DELETE /socio/1</b>, se procede a eliminarlo, por ultimo se verifica lo devuelto y si por medio del ORM ya no existen socios cargados.

- Eliminacion de socio no existente: a través del endpoint <b>DELETE /socio/1</b> se procede a eliminar el socio con id 1 (inexistente), por lo que se verifica que la salida sea adecuada a un mensaje de no encontrado.
