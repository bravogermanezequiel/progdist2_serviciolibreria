from app import db

class Autor(db.Model):
    __tablename__ = "autores"
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(128))
    apellido = db.Column(db.String(128))
    edad = db.Column(db.Integer)
    
    def __init__(self, nombre="", apellido="", edad=""):
        self.nombre = nombre
        self.apellido = apellido
        self.edad = edad
    
class Genero(db.Model):
    __tablename__ = "generos"
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(128))
    
    def __init__(self, nombre=""):
        self.nombre = nombre

class Editorial(db.Model):
    __tablename__ = "editoriales"
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(128))
    
    def __init__(self, nombre=""):
        self.nombre = nombre

class Libro(db.Model):
    __tablename__ = "libros"
    id = db.Column(db.Integer, primary_key=True)
    titulo = db.Column(db.String(128))
    autorId = db.Column(db.Integer, db.ForeignKey("autores.id"))
    generoId = db.Column(db.Integer, db.ForeignKey("generos.id"))
    editorialId = db.Column(db.Integer, db.ForeignKey("editoriales.id"))
    volumen = db.Column(db.Integer)

    def __init__(self, titulo="", autorId=0, generoId=0, editorialId=0, volumen=1):
        self.titulo = titulo
        self.autorId = autorId
        self.generoId = generoId
        self.editorialId = editorialId
        self.volumen = volumen
        
class Socio(db.Model):
    __tablename__ = "socios"
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(128))
    apellido = db.Column(db.String(128))
    edad = db.Column(db.Integer)
    
    def __init__(self, nombre="", apellido="", edad=""):
        self.nombre = nombre
        self.apellido = apellido
        self.edad = edad