from app import initApp, db
from entidades import Autor, Genero, Editorial
import os
from config_log import CONFIG_LOGGING
import logging
import logging.config

def dummyPopulate(db):
    autores = [Autor("Isaac", "Asimov", 89), Autor("Peter", "Sczaskiw", 72), Autor("Rubin", "Persel", 52)]
    for autor in autores:
        db.session.add(autor)
        db.session.commit()
    
    generos = [Genero("Policial"), Genero("Ciencia ficcion"), Genero("Romantico")]
    for genero in generos:
        db.session.add(genero)
        db.session.commit()
    
    editoriales = [Editorial("Nova"), Editorial("Larousse"), Editorial("Librito S.A.")]
    for editorial in editoriales:
        db.session.add(editorial)
        db.session.commit()

logging.config.dictConfig(CONFIG_LOGGING)
app = initApp()

if __name__ == '__main__':
    dbExists = os.path.isfile('./db/sqlite.db')

    with app.app_context():
        db.create_all()
        # Si la tabla SQLite no existia antes de llamar al metodo create_all, cargo la dummy data basica para poder interactuar con la API sin entrar en excepciones constantes.
        if not dbExists:
            dummyPopulate(db)

    app.run(host="0.0.0.0", port=3000)