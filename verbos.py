from flask import request, Blueprint
from repositorio import *
from excepciones import *
import logging

HTTP_OK_CREATED = 201
HTTP_OK_REQUEST = 200
HTTP_BAD_REQUEST = 400
HTTP_NOT_FOUND = 404

ruta = Blueprint("rutas", __name__, url_prefix="")

@ruta.route("/", methods=["GET"])
def server_info():
    return jsonify(RootRepositorio().get()), HTTP_OK_REQUEST

@ruta.route("/autores/", methods=["GET"])
def listAutores():
    return jsonify(AutorRepositorio().list()), HTTP_OK_REQUEST

@ruta.route("/autor/<int:id>", methods=["GET"])
def getAutor(id):
    try:
        autor = AutorRepositorio().get(id)
        return jsonify(autor), HTTP_OK_REQUEST
    except NotFoundException as e:
        return jsonify({ "Resultado": "No encontrado"}), HTTP_NOT_FOUND

@ruta.route("/generos/", methods=["GET"])
def listGeneros():
    return jsonify(GeneroRepositorio().list()), HTTP_OK_REQUEST

@ruta.route("/genero/<int:id>", methods=["GET"])
def getGenero(id):
    try:
        genero = GeneroRepositorio().get(id)
        return jsonify(genero), HTTP_OK_REQUEST
    except NotFoundException as e:
        return jsonify({ "Resultado": "No encontrado"}), HTTP_NOT_FOUND

@ruta.route("/editoriales/", methods=["GET"])
def listEditoriales():
    return jsonify(EditorialRepositorio().list()), HTTP_OK_REQUEST

@ruta.route("/editorial/<int:id>", methods=["GET"])
def getEditorial(id):
    try:
        editorial = EditorialRepositorio().get(id)
        return jsonify(editorial), HTTP_OK_REQUEST
    except NotFoundException as e:
        return jsonify({ "Resultado": "No encontrado"}), HTTP_NOT_FOUND

@ruta.route("/libros/", methods=["GET"])
def listLibros():
    return jsonify(LibroRepositorio().list()), HTTP_OK_REQUEST

@ruta.route("/libro/<int:id>", methods=["GET"])
def getLibro(id):
    try:
        libro = LibroRepositorio().get(id)
        return jsonify(libro), HTTP_OK_REQUEST
    except NotFoundException as e:
        return jsonify({ "Resultado": "No encontrado"}), HTTP_NOT_FOUND

@ruta.route("/libro/", methods=["POST"])
def createLibro():
    try:
        resp = LibroRepositorio().create()
        return jsonify(resp), HTTP_OK_CREATED
    except BadRequestException as e:
        return jsonify({"Resultado": str(e)}), HTTP_BAD_REQUEST
    
@ruta.route("/libro/<int:id>", methods=["PUT"])
def updateLibro(id):
    try:
        resp = LibroRepositorio().update(id)
        return jsonify(resp), HTTP_OK_REQUEST
    except NotFoundException:
        return jsonify({ "Resultado": "No encontrado" }), HTTP_NOT_FOUND
    except BadRequestException as e:
        return jsonify({ "Resultado": str(e) }), HTTP_BAD_REQUEST

@ruta.route("/libro/<int:id>", methods=["DELETE"])
def deleteLibro(id):
    try:
        resp = LibroRepositorio().delete(id)
        return jsonify(resp), HTTP_OK_REQUEST
    except NotFoundException:
        return jsonify({ "Resultado": "No encontrado" }), HTTP_NOT_FOUND
    except BadRequestException as e:
        return jsonify({ "Resultado": str(e) }), HTTP_BAD_REQUEST

@ruta.route("/socios/", methods=["GET"])
def listSocios():
    return jsonify(SocioRepositorio().list()), HTTP_OK_REQUEST

@ruta.route("/socio/<int:id>", methods=["GET"])
def getSocio(id):
    try:
        socio = SocioRepositorio().get(id)
        return jsonify(socio), HTTP_OK_REQUEST
    except NotFoundException:
        return jsonify({ "Resultado": "No encontrado" }), HTTP_NOT_FOUND

@ruta.route("/socio/", methods=["POST"])
def createSocio():
    return jsonify(SocioRepositorio().create()), HTTP_OK_CREATED

@ruta.route("/socio/<int:id>", methods=["PUT"])
def updateSocio(id):
    try:
        resp = SocioRepositorio().update(id)
        return jsonify(resp), HTTP_OK_REQUEST
    except NotFoundException:
        return jsonify({ "Resultado": "No encontrado" }), HTTP_NOT_FOUND

@ruta.route("/socio/<int:id>", methods=["DELETE"])
def deleteSocio(id):
    try:
        resp = SocioRepositorio().delete(id)
        return jsonify(resp), HTTP_OK_REQUEST
    except NotFoundException:
        return jsonify({ "Resultado": "No encontrado" }), HTTP_NOT_FOUND



@ruta.before_request
def beforeRequest():
    log = logging.getLogger('RECEIVEREQUEST_LOGGING')
    endpoint = request.path
    method = request.method
    body = request.data.replace(b'\n', b'').decode('utf-8')
    log.info(f"[Entrante] Request. request.method={method} request.endpoint={endpoint} request.body='{body}'")

@ruta.after_request
def afterRequest(response):
    log = logging.getLogger('RECEIVEREQUEST_LOGGING')
    endpoint = request.path
    method = request.method
    data = response.get_data().replace(b'\n', b'').decode('utf-8')
    log.info(f"[Entrante] Response. response.method={method} response.endpoint={endpoint} response.data='{data}' response.status_code={response.status_code}")
    return response